// Fill out your copyright notice in the Description page of Project Settings.

#include "Shape.h"
#include "Components/SplineComponent.h"

// Sets default values
AShape::AShape()
{
	RootSceneComp = CreateDefaultSubobject<USceneComponent>(TEXT("RootSceneComp"));
	RootComponent = RootSceneComp;

	SplineComp = CreateDefaultSubobject<USplineComponent>(TEXT("SplineComp"));
	//SplineComp->SetDrawDebug(true);
	SplineComp->SetClosedLoop(true, true);
	SplineComp->SetupAttachment(RootSceneComp);
}

// Called when the game starts or when spawned
void AShape::BeginPlay()
{
	Super::BeginPlay();
	
}

bool AShape::CreateShape(TArray<FVector> SplinePointsLocation, UStaticMesh* ShapeMeshReference)
{
	if (ShapeMeshReference == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Creation square shape is impossible, please reference a mesh in the function and not a null pointer"));

		return false;
	}

	PointsLocation = SplinePointsLocation;

	SplineComp->ClearSplinePoints(true);

	for (FVector vect : SplinePointsLocation)
	{
		SplineComp->AddSplineWorldPoint(vect);
	}

	for (int i = 0; i < SplineComp->GetNumberOfSplinePoints(); i++)
	{
		SplineComp->SetSplinePointType(i, ESplinePointType::Curve, true);
	}

	SplineComp->UpdateSpline();

	return true;
}

void AShape::UpdateShapeSize(TArray<FVector> NewPointsLocation)
{
	PointsLocation = NewPointsLocation;
	
	UpdatePointsOnSplineComp(NewPointsLocation);

}

TArray<FVector> AShape::GetPointsLocation()
{
	return PointsLocation;
}

void AShape::UpdatePointsOnSplineComp(TArray<FVector> SplinePointPositions)
{


	for (FVector vectPointPos : SplinePointPositions)
	{
		//DrawDebugSphere(GetWorld(), vectPointPos, 10.f, 20, FColor::Red, false, 0.5f, 0);
		SplineComp->SetSplineWorldPoints(SplinePointPositions);
	}

	SplineComp->UpdateSpline();
}