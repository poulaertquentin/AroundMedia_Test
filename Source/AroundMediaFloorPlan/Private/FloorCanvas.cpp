// Fill out your copyright notice in the Description page of Project Settings.

#include "FloorCanvas.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SceneComponent.h"
#include "Components/BoxComponent.h"
#include "Misc/FileHelper.h"
#include "Misc/Paths.h"
#include "Dom/JsonObject.h"
#include "Serialization/JsonReader.h"
#include "EngineGlobals.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "Engine/DataTable.h"
#include "JsonObjectConverter.h"
#include "Engine/World.h"
#include "SquareShape.h"
#include "EngineUtils.h"
#include "TemplateStorage.h"
#include "Ruler.h"

// Sets default values
AFloorCanvas::AFloorCanvas()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	RootSceneComp = CreateDefaultSubobject<USceneComponent>(TEXT("RootSceneComp"));
	RootComponent = RootSceneComp;

	ColliderComp = CreateDefaultSubobject<UBoxComponent>(TEXT("ColliderComp"));
	ColliderComp->SetRelativeLocation(FVector(50.f, -50.f, -10.f));
	ColliderComp->SetBoxExtent(FVector(50.f, 50.f, 10.f),false);
	ColliderComp->SetCollisionObjectType(ECC_Visibility);
	ColliderComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	ColliderComp->SetCollisionResponseToChannel(ECC_Visibility, ECollisionResponse::ECR_Block);
	ColliderComp->SetupAttachment(RootSceneComp);

	CanvasMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FloorCanvasComp"));
	CanvasMeshComp->SetCollisionObjectType(ECC_Visibility);
	CanvasMeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	CanvasMeshComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	//FloorCanvasComp->Colisi
	CanvasMeshComp->SetupAttachment(ColliderComp);
}


void AFloorCanvas::BeginPlay()
{
	Super::BeginPlay();
	
	//ParseJson(JsonFileName);
	//WriteJson(JsonFileName);

	//Get Template actore
	for (TActorIterator<ATemplateStorage>ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		DataTemplateSotarge = *ActorItr;
		break;
	}

	//CreateRuler();
}

// Called every frame
void AFloorCanvas::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}


FString AFloorCanvas::GetCanvasName()
{
	return CanvasDataStruct.CanvasName;
}

void AFloorCanvas::LoadDataFromFile(FString JsonFileName)
{
	//Clear all
	ClearCanvas();

	//Load new json
	CanvasDataStruct = LoadJson(JsonFileName);
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Canvas name = %s"),*CanvasDataStruct.CanvasName));
	OnNameChanged.Broadcast(CanvasDataStruct.CanvasName);

	//Create shapes
	//Init new array to avoid error (If I read the struc it generates an error)
	TArray<FShapeStruct> Squares = CanvasDataStruct.Squares;
	for (FShapeStruct Square : Squares)
	{
		CreateShape( EShapeEnum::S_Square, Square.Points, DataTemplateSotarge->SquareTemplate.WallMesh, false);
	}

	//Create rulers
	TArray<FRulerStructure> Rulers = CanvasDataStruct.Rulers;
	FRulerStructureTemplate MyTemplate = DataTemplateSotarge->RulerTemplate;
	for (int i = Rulers.Num() - 1; i >= 0; i--)
	{
		MyTemplate.Location = Rulers[i].Location;
		MyTemplate.PointA = Rulers[i].PointA;
		MyTemplate.PointB = Rulers[i].PointB;
		MyTemplate.WidgetText = Rulers[i].WidgetText;
		CreateRuler(MyTemplate);

	}


}

void AFloorCanvas::SaveDataToFile(FString JsonFileName)
{
	WriteJson(JsonFileName, CanvasDataStruct);
}

void AFloorCanvas::ClearCanvas()
{
	//Empty the structure
	CanvasDataStruct.ClearStruct();

	//Clear shapes
	for (ASquareShape* Shape : ShapesArray)
	{
		Shape->Destroy();
	}

	for (ARuler* Ruler: RulersArray)
	{
		Ruler->Destroy();
	}

	//Sent the information for the canvas
	OnNameChanged.Broadcast(CanvasDataStruct.CanvasName);
}

void AFloorCanvas::SetCanvasName(FString NewCanvasName)
{
	CanvasDataStruct.CanvasName = NewCanvasName;
}


void AFloorCanvas::AddShapeInArray(EShapeEnum NewShapeType, ASquareShape* NewShape)
{
	//Store the new shape inside an array
	ShapesArray.Add(NewShape);
}

FFloorCanvasStruct AFloorCanvas::LoadJson(const FString& jsonfile)
{
	FFloorCanvasStruct NewCanvasStruct;

	FString jsonData;
	FString jsonFileName = jsonfile + ".json";
	FString fileName = "Json/" + jsonFileName;
	FString path = FPaths::Combine(*FPaths::ProjectContentDir(), *fileName);
	FFileHelper::LoadFileToString(jsonData, *path);
	
	bool ConvertJsonToStructSuccess = FJsonObjectConverter::JsonObjectStringToUStruct(jsonData, &NewCanvasStruct, 0, 0);

	if (!ConvertJsonToStructSuccess)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Json to struct canvas databe is a failure."));
		
	}
	
	return NewCanvasStruct;
}

bool AFloorCanvas::WriteJson(const FString& jsonfile, FFloorCanvasStruct NewFloorCanvasStruct)
{
	FString jsonData;
	FString jsonFileName = jsonfile + ".json";
	FString fileName = "Json/" + jsonFileName;
	FString path = FPaths::Combine(*FPaths::ProjectContentDir(), *fileName);
	FFileHelper::LoadFileToString(jsonData, *path);

	//Convert struct to json object
	TSharedRef<FJsonObject> outObject(new FJsonObject());
	FString jsonDataString;
	bool SerializeationFromStructureSuccess = FJsonObjectConverter::UStructToJsonObjectString(FFloorCanvasStruct::StaticStruct(), &NewFloorCanvasStruct, jsonDataString, 0, 0, 0);
	if (!SerializeationFromStructureSuccess)
	{
		return false;
	}
	
	//Create json object and fill it with content from structure
	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject);
	JsonObject->SetObjectField("MyObject", outObject);
	
	bool SaveToFileSuccess = FFileHelper::SaveStringToFile(jsonDataString, *path, FFileHelper::EEncodingOptions::ForceUTF8);
	if (!SaveToFileSuccess)
	{
		return false;
	}
	return true;
}



ASquareShape* AFloorCanvas::CreateShape(EShapeEnum ShapeType, FVector SpawnWorldPositon)
{

	TArray<FVector> NewShapePoints;
	UStaticMesh* WallMesh = nullptr;

	switch (ShapeType)
	{
	case EShapeEnum::S_Square:
	{
		
		///TODO: Find an alternative if we don't have the Template storage
		if (DataTemplateSotarge->SquareTemplate.Points.Num() != 4)
		{
			UE_LOG(LogTemp, Error, TEXT("You template storage is not filled or not a square. Check in that actor you template : %s "), *DataTemplateSotarge->GetName());
			return nullptr;
		}

		UE_LOG(LogTemp, Error, TEXT("Spawn world location  : %s "), *SpawnWorldPositon.ToString());

		//Add template data to world position. (For the moment my spline is working in world position, Update that later if needed)
		FVector TopLeft = SpawnWorldPositon;
		FVector BottomLeft = SpawnWorldPositon + DataTemplateSotarge->SquareTemplate.Points[1];
		FVector BottomRight = SpawnWorldPositon + DataTemplateSotarge->SquareTemplate.Points[2];
		FVector TopRight = SpawnWorldPositon + DataTemplateSotarge->SquareTemplate.Points[3];

		NewShapePoints.Add(TopLeft);
		NewShapePoints.Add(BottomLeft);
		NewShapePoints.Add(BottomRight);
		NewShapePoints.Add(TopRight);

		WallMesh = DataTemplateSotarge->SquareTemplate.WallMesh;
	}
		break;
	case EShapeEnum::S_Triangle:
	{
		UE_LOG(LogTemp, Warning, TEXT("Triangle not manage, please update the code"));
			return nullptr;
	}
		break;
	}
	
	
	return CreateShape(ShapeType, NewShapePoints, WallMesh);
}

ASquareShape* AFloorCanvas::CreateShape(EShapeEnum ShapeType, TArray<FVector> PointsWorldPosition, UStaticMesh* NewWallMesh, bool UpdateDatabase)
{

	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;

	//Create square
	ASquareShape* NewSquareShape;
	NewSquareShape = GetWorld()->SpawnActor<ASquareShape>(PointsWorldPosition[0], Rotation, SpawnInfo);

	if (!NewSquareShape->CreateShape(PointsWorldPosition, NewWallMesh))
	{
		NewSquareShape->Destroy();
		return nullptr;
	}
	
	AddShapeInArray(ShapeType,NewSquareShape);
	
		
	if (UpdateDatabase)
	{
		///TODO: Remove that for here, it is hard to maintain
		FShapeStruct NewSquare;

		switch (ShapeType)
		{
		case EShapeEnum::S_Square:
			NewSquare.ShapeType = "Square";
			break;
		case EShapeEnum::S_Triangle:

			break;
		default:
			break;
		}

		NewSquare.Points = NewSquareShape->GetPointsLocation();;
		CanvasDataStruct.AddShapeStruct(NewSquare);
	}
	
	IsInitialisingShape = true;

	return NewSquareShape;
}

void AFloorCanvas::InitiliseShape(FVector NewMouseWorldLocation)
{
	//Get last shape created 
	ASquareShape* InitializedShape = ShapesArray[ShapesArray.Num() - 1];
	//Get shape points
	TArray<FVector> CurrentShapePoints = InitializedShape->GetPointsLocation();
	FVector OriginShapePoint = CurrentShapePoints[0];
	
	//Get new positions
	FVector TopLeft = OriginShapePoint;
	FVector BottomLeft{OriginShapePoint.X, NewMouseWorldLocation.Y, OriginShapePoint.Z};
	FVector BottomRight{NewMouseWorldLocation.X, NewMouseWorldLocation.Y, OriginShapePoint.Z};
	FVector TopRight{NewMouseWorldLocation.X, OriginShapePoint.Y, OriginShapePoint.Z};
	
	TArray<FVector> NewShapePoints;
	NewShapePoints.Add(TopLeft);
	NewShapePoints.Add(BottomLeft);
	NewShapePoints.Add(BottomRight);
	NewShapePoints.Add(TopRight);
	int indexShape = CanvasDataStruct.Squares.Num() - 1;

	//Update Database
	CanvasDataStruct.UpdateShapeStructPoints(indexShape, NewShapePoints);
	CanvasDataStruct.Squares[CanvasDataStruct.Squares.Num()-1].Points = NewShapePoints;
	InitializedShape->UpdateShapeSize(NewShapePoints);

	InitRuler(InitializedShape->GetWidth(),InitializedShape->GetLenght(), InitializedShape->GetSquareMeter());
	///TODO: add a system with the Width and height
	/*

	float NewHeight = OriginShapePoint.X - NewMouseWorldLocation.X;
	float  NewWidth = OriginShapePoint.Y - NewMouseWorldLocation.Y;
	InitShape->UpdateShapeSize(NewWidth, NewHeight);
	*/
}

void AFloorCanvas::CreateRuler(FVector SpawnWorldPositon)
{
	//Get A template storage
	FRulerStructureTemplate CustomRulerStructTemplate = DataTemplateSotarge->RulerTemplate;

	CustomRulerStructTemplate.Location = SpawnWorldPositon;

	CreateRuler(CustomRulerStructTemplate);
}

void AFloorCanvas::CreateRuler(FRulerStructureTemplate CustomRulerStructTemplate, bool UpdateDatabase)
{
	FRotator SpawnWorldRotation{ 0.f,0.f,0.f };
	FActorSpawnParameters SpawnParam;

	ARuler* NewRuler = GetWorld()->SpawnActor<ARuler>(CustomRulerStructTemplate.Location, SpawnWorldRotation, SpawnParam);
	
	NewRuler->SetRulerDataStructureTemplate(CustomRulerStructTemplate);

	//Add to ruler array (Keep ref of the ruler in canvas)
	RulersArray.Add(NewRuler);
	
	if (UpdateDatabase)
	{
		//Add to database
		CanvasDataStruct.AddRuler(CustomRulerStructTemplate);
	}

}

void AFloorCanvas::InitRuler(float NewWidth, float NewLenght, float SquareMeter)
{

	//Get last ruler created
	ARuler* ToInitilizeRuler = RulersArray[RulersArray.Num() - 1];
	
	///TODO: Remove virgules
	//int FlooredString = FMath::FloorToFloat(NewHeight * 100);
	
	int nbOfChar = 5;

	FString NewWidthString = FString::SanitizeFloat(NewWidth);
	NewWidthString.RemoveAt(nbOfChar, NewWidthString.GetCharArray().Num() - nbOfChar);
	FString NewLenghtString = FString::SanitizeFloat(NewLenght);
	NewLenghtString.RemoveAt(nbOfChar, NewLenghtString.GetCharArray().Num() - nbOfChar);
	FString NewSquareMeter = FString::SanitizeFloat(SquareMeter);
	NewSquareMeter.RemoveAt(nbOfChar, NewSquareMeter.GetCharArray().Num() - nbOfChar);

	FString NewTextForRuler = "W: " + NewWidthString  + " /L: " + NewLenghtString + " /SqrM: " + NewSquareMeter;
	FRulerStructure NewRulerStructure = ToInitilizeRuler->GetRulerDataStructure();
	NewRulerStructure.WidgetText = NewTextForRuler;
	ToInitilizeRuler->SetRulerDataStructure(NewRulerStructure);

	//Update database here
	int index = CanvasDataStruct.Rulers.Num() - 1;
	CanvasDataStruct.UpdateRulerText(index, NewTextForRuler);
}

