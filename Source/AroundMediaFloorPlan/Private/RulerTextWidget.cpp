// Fill out your copyright notice in the Description page of Project Settings.

#include "RulerTextWidget.h"




FString URulerText::GetMeasureText()
{
	return MeasureText;
}

void URulerText::SetMeasureText(FString NewMeasureText)
{

	
	MeasureText = NewMeasureText;

	TextSettedEvent.Broadcast();
}

void URulerText::NativeConstruct()
{
	Super::NativeConstruct();
}
