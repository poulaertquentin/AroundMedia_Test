
// Fill out your copyright notice in the Description page of Project Settings.

#include "Ruler.h"
#include "Components/SplineComponent.h"
#include "Components/SplineMeshComponent.h"
#include "RulerTextWidget.h"
#include "Components/WidgetComponent.h"
#include  "UObject/ConstructorHelpers.h"
// Sets default values
ARuler::ARuler()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SplineBaseComp = CreateDefaultSubobject<USplineComponent>(TEXT("SplineBase"));
	SplineBaseComp->ClearSplinePoints(true);
	SplineBaseComp->AddSplineWorldPoint(RulerDataStructure.PointA);
	SplineBaseComp->AddSplineWorldPoint(RulerDataStructure.PointB);
	RootComponent = SplineBaseComp;
	SplineBaseComp->UpdateSpline();

	MeshSplineComp = CreateDefaultSubobject<USplineMeshComponent>(TEXT("SplineMeshComp"));
	MeshSplineComp->SetStaticMesh(LineStaticMesh);
	MeshSplineComp->SetMobility(EComponentMobility::Movable);
	MeshSplineComp->SetForwardAxis(ESplineMeshAxis::Y);
	
	FVector2D RulerMeshScale(0.1f, 0.1f);
	MeshSplineComp->SetStartScale(RulerMeshScale);
	MeshSplineComp->SetEndScale(RulerMeshScale);
	UpdateMeshSplineCompPoints();
	MeshSplineComp->SetupAttachment(SplineBaseComp);

	SceneRootWidgetComp = CreateDefaultSubobject<UWidgetComponent>(TEXT("RootWidgetComp"));
	SceneRootWidgetComp->SetupAttachment(SplineBaseComp);

	DistanceTextWidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("DistanceWidgetComp"));
	DistanceTextWidgetComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	DistanceTextWidgetComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	DistanceTextWidgetComponent->SetupAttachment(SplineBaseComp);
	FVector Scale{ 0.2f,0.2f,0.2f };
	DistanceTextWidgetComponent->SetRelativeScale3D(Scale);
	DistanceTextWidgetComponent->SetDrawSize(FVector2D{ 3000, 250});
	FRotator InitRotation{ 90.f, 180.f, 0.f };
	DistanceTextWidgetComponent->SetRelativeRotation(InitRotation);
	
}

// Called when the game starts or when spawned
void ARuler::BeginPlay()
{
	Super::BeginPlay();
	
	//Bind event 
	OnStructureChanged.AddDynamic(this, &ARuler::OnStuctureUpdated);
}

FRulerStructure ARuler::GetRulerDataStructure()
{
	return RulerDataStructure;
}

void ARuler::SetRulerDataStructure(FRulerStructure NewStructure)
{
	RulerDataStructure = NewStructure;

	OnStructureChanged.Broadcast(false);
}

void ARuler::SetRulerDataStructureTemplate(FRulerStructureTemplate NewStructure)
{
	LineStaticMesh = NewStructure.BaseMesh;
	TypeWidget = NewStructure.UserWidgetType;

	//Wrap template to normal struct
	///TODO: Add that inside the structure
	FRulerStructure RulerStructure;
	RulerStructure.Location = NewStructure.Location;
	RulerStructure.PointA = NewStructure.PointA;
	RulerStructure.PointB = NewStructure.PointB;
	RulerStructure.WidgetText = NewStructure.WidgetText;
	
	RulerDataStructure = RulerStructure;
	OnStructureChanged.Broadcast(true);

	//SetRulerDataStructure(RulerStructure);
}

void ARuler::OnStuctureUpdated(bool IsCreated)
{
	UE_LOG(LogTemp, Warning, TEXT("Event Launched"));

	// Update spline points
	UpdateSplineCompPoints();

	// Update Mesh spline
	UpdateMeshSplineCompPoints();

	//Update mesh
	///TODO: Find a ways to put a condition to avoid update spline mesh each time
	//UpdateMeshSplineCompMesh();

	//Update global Position
	this->SetActorLocation(RulerDataStructure.Location);


	///TODO: Create a function with that inforamtion
	// Update Ruler Text Widget
	//Calculate distance
	//float distance = (RulerDataStructure.PointA - RulerDataStructure.PointB).Size();
	FString TextContent = RulerDataStructure.WidgetText;

	if (IsCreated)
	{
		DistanceTextWidgetComponent->SetWidgetClass(TypeWidget);
		//Set widget class
	}
	UUserWidget* CurrentWidget = DistanceTextWidgetComponent->GetUserWidgetObject();
	URulerText* Text = Cast<URulerText>(CurrentWidget);
	Text->SetMeasureText(TextContent);
	Text->SetVisibility(ESlateVisibility::HitTestInvisible);

	DistanceTextWidgetComponent->UpdateWidget();
}

void ARuler::UpdateSplineCompPoints()
{
	//Create an array of points
	TArray<FVector> NewPointsLocations;
	NewPointsLocations.Add(RulerDataStructure.PointA);
	NewPointsLocations.Add(RulerDataStructure.PointB);
	//Update points
	SplineBaseComp->SetSplineWorldPoints(NewPointsLocations);
}

void ARuler::UpdateMeshSplineCompPoints()
{
	FVector pointLocationStart, pointTangentStart, pointLocationEnd, pointTangentEnd;

	SplineBaseComp->GetLocalLocationAndTangentAtSplinePoint(0, pointLocationStart, pointTangentStart);
	SplineBaseComp->GetLocalLocationAndTangentAtSplinePoint(1, pointLocationEnd, pointTangentEnd);

	MeshSplineComp->SetStartAndEnd(pointLocationStart, pointTangentStart, pointLocationEnd, pointTangentEnd);
}

void ARuler::UpdateMeshSplineCompMesh()
{
	MeshSplineComp->SetStaticMesh(LineStaticMesh);
	UpdateMeshSplineCompPoints();
}

