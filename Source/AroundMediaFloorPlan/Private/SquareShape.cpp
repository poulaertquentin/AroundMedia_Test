// Fill out your copyright notice in the Description page of Project Settings.

#include "SquareShape.h"
#include "Components/SplineComponent.h"
#include "Components/SceneComponent.h"
#include "Components/SplineMeshComponent.h"
#include "Components/StaticMeshComponent.h"
#include "DrawDebugHelpers.h"
#include "Materials/MaterialInstanceDynamic.h"


// Sets default values
ASquareShape::ASquareShape()
{
	
}

// Called when the game starts or when spawned
void ASquareShape::BeginPlay()
{
	Super::BeginPlay();
}


float ASquareShape::GetWidth()
{
	float WidthCM = FVector::Dist(GetPointsLocation()[0], GetPointsLocation()[3]);
	return  WidthCM / 100;;
}

float ASquareShape::GetLenght()
{
	float LenghtCm = FVector::Dist(GetPointsLocation()[0], GetPointsLocation()[1]);
	return	LenghtCm / 100;
}

float ASquareShape::GetSquareMeter()
{
	return GetWidth() * GetLenght();
}

bool ASquareShape::CreateShape(TArray<FVector> SplinePointPositions, UStaticMesh* WallMeshReference)
{
	Super::CreateShape(SplinePointPositions, WallMeshReference);
	
	ConstructMeshBasedOnSpline(WallMeshReference);

	return true;
}

void ASquareShape::UpdateShapeSize(TArray<FVector> NewPoints)
{
	Super::UpdateShapeSize(NewPoints);
	
	UpdateMeshBasedOnSpline();
}

void ASquareShape::ConstructMeshBasedOnSpline(UStaticMesh* WallMeshReference)
{

	//Clear the table of spline meshes
	for (USplineMeshComponent* meshSplineComp: SplineMeshsArray)
	{
		meshSplineComp->DestroyComponent(true);
	}

	//Clear array
	SplineMeshsArray.Empty();

	for (int i = 0; i < SplineComp->GetNumberOfSplinePoints(); i++)
	{
		//Set mesh different mesh name for each mesh
		FString MeshName = "MeshName" + FString::FromInt(i);

		//Create mesh
		USplineMeshComponent* SplineMesh = NewObject<USplineMeshComponent>(this, FName(*MeshName));
		SplineMesh->SetStaticMesh(WallMeshReference);
		SplineMesh->SetupAttachment(RootSceneComp);
		SplineMesh->SetMobility(EComponentMobility::Movable);
		SplineMesh->SetForwardAxis(ESplineMeshAxis::X);

		//Set initial scale
		FVector2D WallScale(1, 1);
		SplineMesh->SetStartScale(WallScale);
		SplineMesh->SetEndScale(WallScale);

		//Get positions and tangents based on spline
		FVector pointLocationStart, pointTangentStart, pointLocationEnd, pointTangentEnd;
		SplineComp->GetLocalLocationAndTangentAtSplinePoint(i, pointLocationStart, pointTangentStart);
		//Check if the last point
		if (i != SplineComp->GetNumberOfSplinePoints())
		{
			SplineComp->GetLocalLocationAndTangentAtSplinePoint(i + 1, pointLocationEnd, pointTangentEnd);
		}
		else
		{
			SplineComp->GetLocalLocationAndTangentAtSplinePoint(0, pointLocationEnd, pointTangentEnd);
		}
		
		//Set spline mesh 
		SplineMesh->SetStartAndEnd(pointLocationStart, pointTangentStart, pointLocationEnd, pointTangentEnd);

		SplineMeshsArray.Add(SplineMesh);
	}

	RegisterAllComponents();
}

void ASquareShape::UpdateMeshBasedOnSpline()
{
	for (int i = 0; i < SplineMeshsArray.Num(); i++)
	{
		USplineMeshComponent* SplineMesh = SplineMeshsArray[i];
		
		FVector pointLocationStart, pointTangentStart, pointLocationEnd, pointTangentEnd;

		SplineComp->GetLocalLocationAndTangentAtSplinePoint(i, pointLocationStart, pointTangentStart);
		if (i != SplineMeshsArray.Num())
		{
			SplineComp->GetLocalLocationAndTangentAtSplinePoint(i + 1, pointLocationEnd, pointTangentEnd);
		}
		else
		{
			SplineComp->GetLocalLocationAndTangentAtSplinePoint(0, pointLocationEnd, pointTangentEnd);
		}

		SplineMesh->SetStartAndEnd(pointLocationStart, pointTangentStart, pointLocationEnd, pointTangentEnd);
	}
}




 