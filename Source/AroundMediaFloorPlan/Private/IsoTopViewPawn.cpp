// Fill out your copyright notice in the Description page of Project Settings.

#include "IsoTopViewPawn.h"
#include "Camera/CameraComponent.h"
#include "IsoTopPlayerController.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "SquareShape.h"
#include "FloorCanvas.h"


// Sets default values
AIsoTopViewPawn::AIsoTopViewPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComp"));
	CameraComp->SetProjectionMode(ECameraProjectionMode::Orthographic);
	CameraComp->SetOrthoWidth(2000.f);
	CameraComp->bUsePawnControlRotation = true;
	CameraComp->SetAspectRatio(1.7777f);
	CameraComp->SetConstraintAspectRatio(true);

	RootComponent = CameraComp;
}

// Called when the game starts or when spawned
void AIsoTopViewPawn::BeginPlay()
{
	Super::BeginPlay();
	
	PlayerController = Cast<AIsoTopPlayerController>(GetController());
}

// Called every frame
void AIsoTopViewPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);



	//Update spawn 
	if (IsSpawningShape)
	{
		FHitResult HitCanva;
		AFloorCanvas* HittedCanva = LineTraceFromMouseToCanva(HitCanva);
		//
		if (HittedCanva != nullptr)
		{
			HittedCanva->InitiliseShape(HitCanva.ImpactPoint);

			IsSpawningShape = true;
		}
		WasSpawningShapeLastFrame = true;
	}
	else
	{
		WasSpawningShapeLastFrame = false;
	}

}

// Called to bind functionality to input
void AIsoTopViewPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAction("LeftMouseButton", IE_Pressed, this, &AIsoTopViewPawn::SpawnShape);
	InputComponent->BindAction("LeftMouseButton", IE_Released, this, &AIsoTopViewPawn::StopSpawingShape);
}

void AIsoTopViewPawn::SpawnShape()
{

	if (PlayerController->SelectedShapeMenu != EShapeEnum::S_None)
	{
		//If canvas is touched by mouse ray, create a shape
		FHitResult HitCanva;
		AFloorCanvas* HittedCanva = LineTraceFromMouseToCanva(HitCanva);

		if (HittedCanva != nullptr)
		{
			ASquareShape* ShapeCreated = HittedCanva->CreateShape(PlayerController->SelectedShapeMenu, HitCanva.ImpactPoint);

			float X = (HitCanva.ImpactPoint.X + 50.f);
			float Y = (HitCanva.ImpactPoint.Y + 50.f);
			float Z = (HitCanva.ImpactPoint.Z + 5.f);
			FVector NewVect{ X, Y , Z};
			HittedCanva->CreateRuler(NewVect);
		
			IsSpawningShape = true;
		}
	}
}

void AIsoTopViewPawn::StopSpawingShape()
{
	IsSpawningShape = false;
	CurrentSquareShape = nullptr;
}

AFloorCanvas* AIsoTopViewPawn::LineTraceFromMouseToCanva(FHitResult &OutHit)
{
	PlayerController->DeprojectMousePositionToWorld(MousePosition, MouseDirection);

	FVector StartLine = MousePosition;
	FVector EndLine = FVector(StartLine.X, StartLine.Y, StartLine.Z - 2000);

	DrawDebugLine(GetWorld(), StartLine, EndLine, FColor::Red, false, 1.0f, 0, 1.0f);

	//FHitResult Hit;
	
	///TODO: Change ECC visibility to avoid other objects 
	if (GetWorld()->LineTraceSingleByChannel(OutHit, StartLine, EndLine, ECC_Visibility))
	{
		
		AFloorCanvas* FloorCanvasHitted = Cast<AFloorCanvas>(OutHit.GetActor());

		if (FloorCanvasHitted != nullptr)
		{
			return FloorCanvasHitted;
		}
	}
	LastLeftMouseClickPosition = MousePosition;
	return nullptr;
}

