// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DataStructures.h"
#include "TemplateStorage.generated.h"

/**
* Store all template in your scene with Floor canvas.
* Allow game designer to edit information inside the scene.
*/
UCLASS()
class AROUNDMEDIAFLOORPLAN_API ATemplateStorage : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATemplateStorage();
	
	/** ruler tempalte */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="StructTemplate")
	FRulerStructureTemplate RulerTemplate;
	
	/** Square template */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "StructTemplate")
	FSquareShapeStructTemplate SquareTemplate;
	
	
};
