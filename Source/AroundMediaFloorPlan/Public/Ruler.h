// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DataStructures.h"
#include "Ruler.generated.h"



class USplineComponent;
class USplineMeshComponent;
class URulerText;
class UWidgetComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FStructureChangeSignature, bool, IsCreated);

/**
* Ruler gives visual information to the user about shapes.
* For the moment only canvas is dispalyed (Spline system is not working)
* This class is only editable through it's structure. 
*
*/
UCLASS()
class AROUNDMEDIAFLOORPLAN_API ARuler : public AActor
{
	GENERATED_BODY()

		//~=============================================================================
		// Override functions.

public:	
	// Sets default values for this actor's properties
	ARuler();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	//~=============================================================================
	// Datas & Structures 

public:

	/**
	* Get data structure of this Ruler

	*/
	UFUNCTION()
	FRulerStructure GetRulerDataStructure();

	/**
	 * Update the ruler with a data structure

	 @param New Ruler data structure
	 */
	UFUNCTION()
	void SetRulerDataStructure(FRulerStructure NewStructure);
	
	/**
	* Update the ruler with a data structure template. Mesh update will be done.

	@param New Ruler data structure
	*/
	UFUNCTION()
	void SetRulerDataStructureTemplate(FRulerStructureTemplate NewStructure);
	
protected:
	/**
	* Data structure to keep ruller info
	*/
	UPROPERTY(EditAnywhere, Category = "RulerData")
	FRulerStructure RulerDataStructure;
	/**
	* Type widget to instanciate in the widget component returned from RulerStructureTemplate
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RulerComp")
	TSubclassOf<URulerText> TypeWidget;
	/**
	 * Static mesh used on the line ruler (Not implemented yet)
	 */
	UPROPERTY(EditDefaultsOnly, Category = "Ruler")
	UStaticMesh* LineStaticMesh;

	//~=============================================================================
	// Ruler behaviour

private:

	/**
	 * Update the ruler behavior each time the structure has been changed. 
	 * Linked to the event on structure changed
	 */
	UFUNCTION()
	void OnStuctureUpdated(bool IsCreated);

	/**
	 * Update points of the spline component based on the structure
	 *
	 * Not used for the moment.
	 */
	UFUNCTION()
	void UpdateSplineCompPoints();


	/**
	* Update points of the mesh component based on structure
	*
	* Not used for the moment.
	*/
	UFUNCTION()
	void UpdateMeshSplineCompPoints();

	/**
	* Update mesh of the mesh componnet based on structure
	*
	* Not used for the moment.
	*/
	UFUNCTION()
	void UpdateMeshSplineCompMesh();

public:

	//~=============================================================================
	// Blueprint linked event.
	UPROPERTY(BlueprintAssignable, Category = "Ruler")
		FStructureChangeSignature OnStructureChanged;


	//~=============================================================================
	// Components
protected:

	UPROPERTY(EditAnywhere, Category="RulerComp")
	USplineComponent* SplineBaseComp;
	UPROPERTY(EditDefaultsOnly, Category = "RulerComp")
	USplineMeshComponent* MeshSplineComp;
	UPROPERTY(EditAnywhere, Category = "RulerComp")
	USceneComponent* SceneRootWidgetComp;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RulerComp")
	UWidgetComponent* DistanceTextWidgetComponent;



};
