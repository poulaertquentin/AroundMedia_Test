// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "IsoTopViewPawn.generated.h"

class UCameraComponent;
class AIsoTopPlayerController;
class ASquareShape;
class AFloorCanvas;

UCLASS()
class AROUNDMEDIAFLOORPLAN_API AIsoTopViewPawn : public APawn
{
	GENERATED_BODY()
		//~=============================================================================
		// Override functions.
public:
	// Sets default values for this pawn's properties
	AIsoTopViewPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	//~=============================================================================
	// Components.
protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Camera")
	UCameraComponent* CameraComp;

	//~=============================================================================
	// Canvas spawn behavior.
protected:
	UPROPERTY()
	FVector MousePosition;
	UPROPERTY()
	FVector MouseDirection;
	UPROPERTY()
	FVector LastLeftMouseClickPosition;

private:

	/*
	*Spawn square shape inside the canvas
	*/
	void SpawnShape();

	/*
	*Spawn square shape inside the canvas
	*/
	void StopSpawingShape();

	/**
	 * Trace line based on Player controller to check if we hit canvas
	 @Param Out hit result
	 */
	AFloorCanvas* LineTraceFromMouseToCanva(FHitResult &OutHit);
	

	bool IsSpawningShape = false;
	bool WasSpawningShapeLastFrame = false;

	ASquareShape* CurrentSquareShape = nullptr;

	bool bMouseRayColidedWithActor = false;
	
	/** User player controller*/
	AIsoTopPlayerController* PlayerController;

	FHitResult ResultFromMouseLineTrace;
	

};
