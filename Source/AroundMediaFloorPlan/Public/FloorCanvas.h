// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DataStructures.h"
#include "FloorCanvas.generated.h"


UENUM(BlueprintType)		
enum class EShapeEnum : uint8
{
	S_None	UMETA(DisplayName = "None"),
	S_Square 	UMETA(DisplayName = "Square"),
	S_Triangle	UMETA(DisplayName = "Triangle"),
};

class UStaticMeshComponent;
class USceneComponent;
class UBoxComponent;
class ASquareShape;
class ATemplateStorage;
class ARuler;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCanvasNameChangedSignature, FString, CanvasName);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FSpawnRulerSignature, ASquareShape*, ShapeAttached);

/**
* Floor Canvas is an actor with the objective to manage all shapes of the level.
* Floor Canvas receives information from the pawn to trigger information.
*
*/
UCLASS()
class AROUNDMEDIAFLOORPLAN_API AFloorCanvas : public AActor
{
	GENERATED_BODY()
	
	//~=============================================================================
	// Override functions.

public:	
	// Sets default values for this actor's properties
	AFloorCanvas();

protected:
	/** Overridable native event for when play begins for this actor. */
	virtual void BeginPlay() override;

	/** Overridable native called every frame. */
	virtual void Tick(float DeltaTime) override;

	//~=============================================================================
	// Data functions.
	
public:
	/**Get name of canvas from structure */
	UFUNCTION(BlueprintPure, Category = "Canvas")
	FString GetCanvasName();
	
	/** Event Called when name change in c++ (Changing the variable name would be good) */
	UPROPERTY(BlueprintAssignable, Category = "CanvasEvent")
	FCanvasNameChangedSignature OnNameChanged;
	

	/**
		Pointer to the template storage. 
		The pointer is loaded on begin play
		cf TemplateStorage.h
	*/
	UPROPERTY(EditAnywhere, Category = "CanvasDatasTemplate")
	ATemplateStorage* DataTemplateSotarge;


protected:
	/**
	Load data from the selected json file.

	The file must be located in folder Content/Json/
	@param Json file name (without the .json)
	*/
	UFUNCTION(BlueprintCallable, Category = "Canvas")
	void LoadDataFromFile(FString JsonFileName);
	
	/**
	Save data to the selected json file.

	The file must be located in folder Content/Json/
	@param Json file name (without the .json)
	*/
	UFUNCTION(BlueprintCallable, Category = "Canvas")
	void SaveDataToFile(FString JsonFileName);

	///TODO: Add a canvas 

	/**
	 * Clear the canvas from all shapes and rulers
	 */
	UFUNCTION(BlueprintCallable, Category = "Canvas")
	void ClearCanvas();

	/**
	 * Set canvas name in the canvas data structure.
	 */
	UFUNCTION(BlueprintCallable, Category = "Canvas")
	void SetCanvasName(FString NewCanvasName);


protected:
	/** Json file name used to load template data*/
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "CanvasDatas")
	FFloorCanvasStruct CanvasDataStruct;
	
	/** Json file name where the user data is stored*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CanvasDatas")
	FString UserJsonFileName = "userdata";

	/** Json file name used to load template data*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CanvasDatas")
	FString TemplateJsonFileName = "template";


	/** Shape created
	(TODO change the name of "ASquareShape to AShape"	
	*/
	UPROPERTY(VisibleAnywhere, Category = "CanvasDatas")
	TArray<ASquareShape*> ShapesArray;

	/** Ruler created (Unused for the moment)*/
	UPROPERTY(VisibleAnywhere, Category = "CanvasDatas")
	TArray<ARuler*> RulersArray;

	//Reference mesh for the wall shape
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ShapeCanvas")
	UStaticMesh* WallShape;

	/**Add shape to shape array */
	UFUNCTION(BlueprintCallable, Category = "Canvas")
	void AddShapeInArray(EShapeEnum ShapeType, ASquareShape* NewShape);

	//~=============================================================================
	// Json functions. (There functions must be moved inside another actor)

private:
	/**
	 * Load json from file.
	 
	 Load json from the following folder : /Content/Json. The folder must be accessible after packed game.
	 @param json file name.
	 */
	FFloorCanvasStruct LoadJson(const FString& jsonfile);
	/**
	* Write json from file

	Load json from the following folder : /Content/Json. The folder must be accessible after packed game.
	@param json file name.
	*/
	bool WriteJson(const FString& jsonfile, FFloorCanvasStruct NewFloorCanvasStruct);


	//~=============================================================================
	// Actor Components.

protected:
	/**Base scene component*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "CanvasBaseComponents")
		USceneComponent* RootSceneComp;
	/**Collider reacting to the line traced by the pawn (I could remove that one if I work with visibility)  */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "CanvasBaseComponents")
		UBoxComponent* ColliderComp;
	/**Mesh displaying the canvas in the scene */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "CanvasBaseComponents")
		UStaticMeshComponent* CanvasMeshComp;

	//~=============================================================================
	// Content creation functions.

public:
	
	/**
	Create Shape based on template data structure
	
	@param Type of shape requested
	@param Spawn Location
	*/
	ASquareShape* CreateShape(EShapeEnum ShapeType, FVector SpawnWorldPositon);

	/**
	 * Create shape based on array
	 
	 @param Shape type requested
	 @param Mesh array of all shape points
	 @param Static mesh of new wall mesh
	 */
	ASquareShape* CreateShape(EShapeEnum ShapeType, TArray<FVector> PointsWorldPosition, UStaticMesh* NewWallMesh, bool UpdateDatabase = true);

	/**
	 * Update the shape while the user is moving the mouse
	 */
	void InitiliseShape(FVector NewMouseWorldLocation);

	/**
	* Create ruler based on template from Template storage
	*/
	void CreateRuler(FVector SpawnWorldPositon);
	
	/**
	* Create ruler based on customized template
	*/
	void CreateRuler(FRulerStructureTemplate CustomRulerStructTemplate, bool UpdateDatabase = true);
	
	/**
	* Update ruler while the user is moving the mouse
	*/
	void InitRuler(float NewWidth, float NewHeight, float SquareMeter);


private:
	bool IsInitialisingShape = false;

};
