// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Shape.generated.h"

class USplineComponent;
class USceneComponent;
class USplineMeshComponent;
class UStaticMeshComponent;

UCLASS(abstract)
class AROUNDMEDIAFLOORPLAN_API AShape : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AShape();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//~=============================================================================
	//Components.
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "SquareShapeComp")
		USceneComponent* RootSceneComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "SquareShapeComp")
		USplineComponent* SplineComp;

	//~=============================================================================
	// Square behaviour.

	virtual bool CreateShape(TArray<FVector> SplinePointsLocation, UStaticMesh* ShapeMeshReference);

	/**
	* Update shape size 

	@param Array of fvector with all points
	*/
	virtual void UpdateShapeSize(TArray<FVector> NewPoints);

	/*Stored points array location*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "SquarePoints")
		TArray<FVector> PointsLocation;
public:
	/**
	* Return the array with points  location
	*/
	TArray<FVector> GetPointsLocation();

protected:


private:
	/**
	* Update points of the spline component
	*/
	void UpdatePointsOnSplineComp(TArray<FVector> SplinePointPositions);

};
