// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ShapeItemWidget.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FButtonShapeSignature, class UShapeItemWidget*, ReturnedShapeItem);


/**
 * 
 */
UCLASS()
class AROUNDMEDIAFLOORPLAN_API UShapeItemWidget : public UUserWidget
{
	GENERATED_BODY()

public:
		UPROPERTY(BlueprintAssignable)
		FButtonShapeSignature OnButtonClickEvent;
		
		UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName = "OnClick"))
		void ReceiveClick();

};
