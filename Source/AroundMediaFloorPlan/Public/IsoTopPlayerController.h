// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "FloorCanvas.h"
#include "IsoTopPlayerController.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FShapeSelectedSignature, EShapeEnum, SelectedShape);

/**
 * 
 */
UCLASS()
class AROUNDMEDIAFLOORPLAN_API AIsoTopPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintAssignable, Category = "Shape")
	FShapeSelectedSignature OnNewShapeSelected;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shape")
	EShapeEnum SelectedShapeMenu = EShapeEnum::S_None;

	UFUNCTION(BlueprintCallable, Category = "Shape")
	void SetSelectedShapeType(EShapeEnum NewShapeTypeSelected);

protected:

};
