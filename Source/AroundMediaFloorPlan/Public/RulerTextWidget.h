// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "RulerTextWidget.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTextChangeSignature);

/**
 * 
 */
UCLASS()
class AROUNDMEDIAFLOORPLAN_API URulerText : public UUserWidget
{
	GENERATED_BODY()
	
public:
	
	UPROPERTY(BlueprintAssignable)
	FTextChangeSignature TextSettedEvent;

	UFUNCTION(BlueprintPure, Category = "RulerText")
	FString GetMeasureText();

	UFUNCTION(BlueprintCallable, Category = "RulerText")
	void SetMeasureText(FString NewMeasureText);
protected:

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "RulerText")
		FString MeasureText;


	virtual void NativeConstruct() override;

};
