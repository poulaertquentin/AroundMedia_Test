// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMesh.h"
#include "RulerTextWidget.h"
#include "DataStructures.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct AROUNDMEDIAFLOORPLAN_API FRulerStructure
{
	GENERATED_USTRUCT_BODY()

public:
	FRulerStructure()
	{
		PointA = FVector{ 10.f, 0.f, 0.f };
		PointB = FVector{ 25.f, 0.f, 0.f };
	}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RulerData")
		FVector Location;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RulerData")
		FString WidgetText;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RulerData")
		FVector PointA;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RulerData")
		FVector PointB;
};

USTRUCT(BlueprintType)
struct AROUNDMEDIAFLOORPLAN_API FRulerStructureTemplate : public FRulerStructure
{
	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RulerData")
		UStaticMesh* BaseMesh;

	//Type of widget to create (allow children blueprint to be spawned)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RulerData")
		TSubclassOf<URulerText> UserWidgetType;
};


USTRUCT(BlueprintType)
struct AROUNDMEDIAFLOORPLAN_API FPointStructure
{
	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PointData")
		FVector PointLocation;
};


USTRUCT(BlueprintType)
struct AROUNDMEDIAFLOORPLAN_API FWallStruct
{
	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CanvasData")
		FPointStructure FirstPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CanvasData")
		FPointStructure SecondPoint;
};



USTRUCT(BlueprintType)
struct AROUNDMEDIAFLOORPLAN_API FShapeStruct
{
	GENERATED_USTRUCT_BODY()

public:

	//Add an enum here to store the shape type.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ShapeSquareData")
		FString ShapeType;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ShapeSquareData")
		TArray<FVector> Points;
};


USTRUCT(BlueprintType)
struct AROUNDMEDIAFLOORPLAN_API FSquareShapeStructTemplate : public FShapeStruct
{
	GENERATED_USTRUCT_BODY()

public:
	FSquareShapeStructTemplate()
	{
		ShapeType = "Square";

		FVector PointTopLeft{0.f, 0.f, 0.f};
		FVector PointBottomLeft{ 10.f, 0.f, 0.f};
		FVector PointBottomRight{ 10.f, 15.f, 0.f};
		FVector PointTopRight{ 0.f, 15.f, 0.f};

		Points = {PointTopLeft, PointBottomLeft, PointBottomRight, PointTopRight };
	}

	/** Reference for the mesh to instantiate for walls*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SquareData")
		UStaticMesh* WallMesh;
};


USTRUCT(BlueprintType)
struct AROUNDMEDIAFLOORPLAN_API FFloorCanvasStruct
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CanvasData")
		FString CanvasName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CanvasData")
		TArray<FShapeStruct> Squares;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CanvasData")
		TArray<FRulerStructure> Rulers;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CanvasData")
		TArray<FWallStruct> Walls;

	void ClearStruct()
	{
		CanvasName.Empty();
		Squares.Empty();
		Rulers.Empty();
		Walls.Empty();
	}

	void AddShapeStruct(FShapeStruct NewShapeStruct)
	{
		///TODO: Avoid children to be serialized
		Squares.Add(NewShapeStruct);
	}

	void UpdateShapeStruct(int indexShape, FShapeStruct NewShapeStruct)
	{
		Squares[indexShape] = NewShapeStruct;
	}
	
	void UpdateShapeStructPoints(int indexShape, TArray<FVector> Points)
	{
		
		Squares[indexShape].Points = Points;
		
	}

	void AddRuler(FRulerStructure NewRulerStruct)
	{
		Rulers.Add(WrapStruct(NewRulerStruct));
	}

	void UpdateRulerText(int index, FString NewText)
	{
		Rulers[index].WidgetText = NewText;		
	}

private:
	/**
	 * Avoid any problem with template (Maybe I could protect specific fields to avoid serialization (to test)
	 */
	FRulerStructure WrapStruct(FRulerStructure ToWrapRulerStruct)
	{
		FRulerStructure WrappedRulerStruct;
		WrappedRulerStruct.PointA = ToWrapRulerStruct.PointA;
		WrappedRulerStruct.PointB = ToWrapRulerStruct.PointB;
		WrappedRulerStruct.Location = ToWrapRulerStruct.Location;

		return WrappedRulerStruct;
	}
};


