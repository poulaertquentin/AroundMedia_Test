// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Shape.h"
#include "SquareShape.generated.h"


class USplineComponent;
class USceneComponent;
class USplineMeshComponent;
class UStaticMeshComponent;


UCLASS()
class AROUNDMEDIAFLOORPLAN_API ASquareShape : public AShape
{
	GENERATED_BODY()
	
	//~=============================================================================
	// Overided functions.
public:	
	// Sets default values for this actor's properties
	ASquareShape();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	
	//~=============================================================================
	// Measures accessors.
public:
	/**Get width of the shape
	
	@return Width in meters
	*/
	virtual float GetWidth();
	/**
	 * Get Height of the shape

	@return Lenght in meters
	 */
	virtual float GetLenght();
	/**
	 * Get Square meter of the shape
	 */
	virtual float GetSquareMeter();


	//~=============================================================================
	// Square behaviour.
public:
	/**
	 * Create the shape based
	 *
	 * @param 
	 */
	bool CreateShape(TArray<FVector> SplinePointPositions, UStaticMesh* WallMeshReference) override;

	/*
	void UpdateShapeSize(float Width, float Height);
	*/

	/**
	 * Update shape size

	 @param Array of fvector with all points
	 */
	void UpdateShapeSize(TArray<FVector> NewPoints) override;

	

	//~=============================================================================
	// Mesh creation on a fly.
protected:

	/*Stored mesh spline created*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SquareShape")
		TArray<USplineMeshComponent*> SplineMeshsArray;
private:

	
	/**
	 * Construct mesh based on spline component
	 */
	void ConstructMeshBasedOnSpline(UStaticMesh* WallMeshReference);
	/**
	 * Update mesh components based on spline component
	 */
	void UpdateMeshBasedOnSpline();


	

};